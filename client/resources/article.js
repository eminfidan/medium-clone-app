export default (axios) => ({
  // get user information
  getArticleList: () => {
    return axios.get(`/article`)
  },
  createArticle: (parameters = {}) => {
    return axios.post(`/article`, { ...parameters })
  },
  getArticle: (articleId, parameters = {}) => {
    return axios.get(`/article/${articleId}`, { ...parameters })
  },
  deleteArticle: (articleId) => {
    return axios.delete(`/article/${articleId}`)
  },
  updateArticle: (articleId, parameters = {}) => {
    return axios.patch(`/article/${articleId}`, { ...parameters })
  },
})
