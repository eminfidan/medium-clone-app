import Article from '~/client/resources/article'

export default (context, inject) => {
  const factories = {
    article: Article(context.$axios),
  }

  // Inject $API
  inject('API', factories)
}
